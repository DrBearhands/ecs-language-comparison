
### Downsides
- Array access enforces run-time check, no way to do it manually or statically.
- Error handling difference between panic and Result forces choice at library rather than application
- Create entity and destroy entity cannot easily benefit from rust's Ownership, because they are easily "forgotten" by calling code while being stored all over as indices in component arrays.
- Allocation is a mess, must RAII, but no constructors, so cannot initialize on heap. Uninitialized memory unsafe and experimental. Ended up 0-initializing and copying from stack to heap (might be optimized by compiler).

## Time taken
Over 5 hours and help from a senior rust developper. He didn't know either.