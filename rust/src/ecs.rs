use usize as Entity;

const MAX_ENTITIES : Entity = 5000;

pub struct Scene {
  entity_counter : Entity,
  free_entities : [Entity; MAX_ENTITIES]
}

// All approaches that don't quite work:
//https://www.joshmcguigan.com/blog/array-initialization-rust/
//https://doc.rust-lang.org/stable/std/mem/union.MaybeUninit.html#method.as_mut_ptr
//https://doc.rust-lang.org/std/boxed/struct.Box.html
pub fn create_scene() -> Box<Scene> {
  let mut scene : Box<Scene> = Box::new(Scene {
    entity_counter: 0,
    free_entities: [0; MAX_ENTITIES]
  });
  let mut ii = 0;
  while ii < MAX_ENTITIES {
    scene.free_entities[ii] = ii;
    ii += 1;
  }

  scene
}

pub fn create_entity(mgr : &mut Scene) -> Entity {
  let val = mgr.free_entities[mgr.entity_counter];
  mgr.entity_counter+=1;
  return val;
}

pub fn destroy_entity(mgr : &mut Scene, e : Entity) {
  mgr.entity_counter-=1;
  mgr.free_entities[mgr.entity_counter] = e;
}