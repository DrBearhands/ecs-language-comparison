mod ecs;

fn main() {
    let mut scene : Box<ecs::Scene> = ecs::create_scene();
    let e1 = ecs::create_entity(&mut scene);
    let e2 = ecs::create_entity(&mut scene);
    ecs::destroy_entity(&mut scene, e1);
    let e3 = ecs::create_entity(&mut scene);

    println!("e1 = {e1}, e2 = {e2}, e3 = {e3}");
}
