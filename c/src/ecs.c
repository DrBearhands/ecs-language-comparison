#include <stdio.h>
#include <stdlib.h>

typedef size_t Entity;

#define MAX_ENTITIES 5000

struct Scene {
  Entity num_popped_entities;
  Entity free_entities_stack[MAX_ENTITIES];
};

struct Scene* create_scene() {
  struct Scene *scene = (struct Scene*)malloc(sizeof(struct Scene));
  scene->num_popped_entities = 0;
  for (size_t ii = 0; ii < MAX_ENTITIES; ++ii) {
    scene->free_entities_stack[ii] = ii;
  }
  return scene;
}

Entity create_entity(struct Scene* scene) {
  if (scene->num_popped_entities >= MAX_ENTITIES) {
    return -1;
  }
  Entity ett = scene->free_entities_stack[scene->num_popped_entities];
  scene->num_popped_entities += 1;
  return ett;
}

Entity branchless_create_entity(struct Scene* scene) {
  const Entity out_of_bounds = scene->num_popped_entities >= MAX_ENTITIES;
  Entity ett = scene->free_entities_stack[scene->num_popped_entities];
  scene->num_popped_entities += !out_of_bounds;
  return ett && (out_of_bounds -1);
}

void destroy_entity(struct Scene* scene, Entity ett) {
  scene->num_popped_entities -= 1;
  scene->free_entities_stack[scene->num_popped_entities] = ett;
}

int main() {
  struct Scene* scene = create_scene();
  Entity ett1 = create_entity(scene);
  Entity ett2 = create_entity(scene);
  destroy_entity(scene, ett1);
}