A comparison of a very basic ECS architecture in various programming languages.


## Rules
- All functions must do the theoretical minimum. E.g., initializing by 0 and then setting the correct value is not allowed, nor is allocating to stack and the copying to heap, nor is recycling reference counting
- No libraries, including standard libraries or prelude. This is about language semantics, not library ecosystem.
- The code must have the following (reference in c)
  - Constants: 
    - MAX_ENTITIES: 5000
  - Types: 
    - Entity: a simple index into an array-like
    - Scene: contains a heap-allocated stack-like structure of entities that is tightly packed in memory, bounded in size by MAX_ENTITIES
  - Functions 
    - create_scene
      - allocates the scene s.t. the entity array is increasing from 0 to MAX_ENTITIES.
      - create_entity: "pops" an item from the entity stack. If there are no more items, return -1 (or wrapping equivalent).
      - (optional) branchless_create_entity: same as create-entity, but making the unrealistic assumption that the out-of-bounds conditional prediction fails often, must be branchless.
      - destroy_entity: pushes a previously popped entity back onto the stack.

## Bonus points
- type-safety guarantees
  - Correctness of branchless_create_entity / equivalence to branching version
  - prevent using entity after destruction

