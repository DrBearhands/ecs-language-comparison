type entity = uint64

const max_entities : entity = 5000

type Scene = object
  num_popped_entities : entity
  free_entities_stack : array[0..(int)max_entities, entity]

proc create_scene(): ptr Scene =
  result = cast[ptr Scene](alloc(sizeof(Scene)))
  result.num_popped_entities = 0
  var ii : entity = 0;
  while (ii < max_entities):
    result.free_entities_stack[ii] = ii
    ii += 1

proc create_entity(scene : ptr Scene): entity =
  if (scene.num_popped_entities >= max_entities):
    return -1
  else:
    result = scene.free_entities_stack[scene.num_popped_entities]
    scene.num_popped_entities += 1

proc destroy_entity(scene : ptr Scene, ett : entity) : void =
  scene.num_popped_entities -= 1
  scene.free_entities_stack[scene.num_popped_entities] = ett

let scene : ptr Scene = create_scene()
let ett1 = create_entity(scene)
let ett2 = create_entity(scene)
destroy_entity(scene, ett1)
let ett3 = create_entity(scene)

echo "e1 = ", ett1, ", e2 = ", ett2, ", e3 = ", ett3