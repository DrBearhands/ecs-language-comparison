### Positives
- Very succinct

### Downsides
- Array bounds check are sometimes checked at compile time, but not clear to programmer
- Generally not as clear what is happening compared to C, Zig. Many 'weird' types
- Had to cast entity to int to use as array index. Not sure what is going on there.

### Time
Took about 1-1.5 hours. This was particularly low because the official site had instructions for using the official docker container.