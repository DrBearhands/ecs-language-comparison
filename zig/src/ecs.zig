const std = @import("std");

const MAX_ENTITIES: u64 = 5000;

const Entity = u64;
const allocator = std.heap.page_allocator;

const Scene = struct { 
  num_popped_entities: Entity = undefined, 
  free_entities_stack: [MAX_ENTITIES]Entity = undefined 
};

fn create_scene() *Scene {
  const scene = allocator.create(Scene) catch unreachable;
  scene.*.num_popped_entities = 0;
  var ii : Entity = 0;
  while (ii < MAX_ENTITIES) : (ii += 1) {
    scene.*.free_entities_stack[ii] = ii;
  }

  return scene;
}

fn create_entity(scene : *Scene) Entity {
  if (scene.*.num_popped_entities >= MAX_ENTITIES) {
    return std.math.maxInt(Entity);
  }
  const ett = scene.*.free_entities_stack[scene.*.num_popped_entities];
  scene.*.num_popped_entities += 1;
  return ett;
}

fn destroy_entity(scene : *Scene, ett : Entity) void {
  scene.*.num_popped_entities -= 1;
  scene.*.free_entities_stack[scene.*.num_popped_entities] = ett;
}

pub fn main() void {
  const scene = create_scene();
  const ett1 = create_entity(scene);
  const ett2 = create_entity(scene);
  destroy_entity(scene, ett1);
  const ett3 = create_entity(scene);
  std.debug.print("Hello, entity #{d}!\n", .{ett2});
  std.debug.print("Hello, entity #{d}!\n", .{ett3});
}