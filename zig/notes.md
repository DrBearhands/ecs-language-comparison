### Positives:
- Zig forced us to explicitely disregard allocation failure.

### Downsides:
- Structs are not guaranteed to be any certain size, array may not be tightly packed. Probably not a problem in practice?


### Questions:
- How are for loops compiled?
- Does it checks array bounds at runtime?


### Time
Took ~2 hours including setup, on docker, from 0 experience.